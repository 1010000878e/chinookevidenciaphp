<!--Extender la masterpage en esta vista--> 
@extends('layouts.masterpage')

@section('contenido')
<form method="POST" action="{{ url('empleados/store') }}"  class="form-horizontal">
@csrf

<fieldset>

<!-- Form Name -->
<legend>Nuevo Empleado</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nombre</label>  
  <div class="col-md-4">
  <input id="textinput" name="nombre" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Apellido</label>  
  <div class="col-md-4">
  <input id="textinput" name="apellido" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Jefe directo">Cargo</label>
  <div class="col-md-5">
    <select id="Jefe directo" name="cargo" class="form-control">
    @foreach($cargos as $cargo)
        <option > {{ $cargo->Title}}</option>
    @endforeach
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Jefe directo</label>
  <div class="col-md-5">
    <select id="selectbasic" name="jefe" class="form-control">
    @foreach($jefes as $jefe)
        <option values="{{$jefe->EmployeeId}}"> {{ $jefe->FirstName}}  {{ $jefe->LastName}} - {{$jefe->Title}}</option>
    @endforeach
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Fecha nacimiento</label>  
  <div class="col-md-5">
  <input id="nacimiento" name="nacimiento" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Fecha contratacion</label>  
  <div class="col-md-5">
  <input id="textinput" name="contrato" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Direccion</label>  
  <div class="col-md-5">
  <input id="textinput" name="dir" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Ciudad</label>  
  <div class="col-md-5">
  <input id="textinput" name="ciudad" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Email</label>  
  <div class="col-md-5">
  <input id="textinput" name="email" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

    <!-- Button -->
    <div class="form-group">
    <label class="col-md-4 control-label" for="">Enviar</label>
    <div class="col-md-4">
        <button  type="submit" id="" name="" class="btn btn-success">Button</button>
    </div>
    </div>

</fieldset>
</form>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>

    //datepicker a algun control
    $( function(){
        $(  "#nacimiento").datepicker();
    });
</script>
@endsection