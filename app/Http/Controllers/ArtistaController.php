<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Artista;
use Illuminate\Support\Facades\Validator;

class ArtistaController extends Controller
{
    //Acciones : metodos en un controlador
    //           asociados a una ruta
    //           la programacion en una caso de uso

    public function index(){

        //obtener los datos utilizar el modelo
        $listado_artistas = Artista::all();
        //presentar la vista con los arista de la base de datos 
       return view('artistas.index')->with ("Artistas", $listado_artistas);
    }
    /**
     *  meustre el formulario de creacion de artista
      */     

    public function create(){
        return view('Artistas.new');

    }

    /** 
     * capturar los datos desde el cliente (formulario)
    */

    
    public function store(Request $request){
       //validacion: paso 1 - establecer reglas de validacion
       //        para cada campo
       $reglas = [
           "nombre_artista" => ['required','alpha','min:3' , 'max:10', 'unique:artists,Name' ]
       ];
       //validacion:paso 2 = crear objeto validador: datos a validar y reglas de validacion
       $validador = validator::make($request->all() , $reglas);

       //validacion: paso 3 - validar y establecer acciones 
       if($validador->fails()){
           return redirect('artistas/create')->withErrors($validador);
           //acciones cuandp la validacion falla
           //redirigir a la vista con el validador
           
       }

       //guardar el artista utlizandpo el modelo:
       $a = new Artista();
       $a->Name = $request->nombre_artista;
       $a->save();


       //mensaje a  la vista
       //redireccionamiento : a la ruta que muetsra a la vista
       //with: crear un flash session: variable session que va a durar solo un request
       //con el nombre "exito"
       return redirect('artistas/create')
       ->with("exito" , "Artista registrado  exitosamente")
       ->with("nombre_artista" , $a->Name);

    }
}
