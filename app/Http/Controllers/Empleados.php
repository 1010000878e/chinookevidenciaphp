<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Empleados extends  Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = \App\Empleados::paginate(5);
        return view('empleados.index')->with("empleados",$empleados);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {

        //seleccionar  los jefes 
        $jefes = \App\Empleados::where('Title' , 'like' , '%Manager')->get();

        //seleccionar los cargos 
        $cargos =\App\Empleados::select('Title')->distinct()->get();
        return view('empleados.new')
        ->with("cargos" , $cargos)
        ->with("jefes" , $jefes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //resibir datos del formulario
        var_dump($_POST);
        echo "<hr/>";
        var_dump($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //seleccionar el empleaod con el id por parametros
        $empleado = \App\Empleados::find($id);
        //var_dump($empleado);
        //mostrar  detalle del empleado es una vista
        return view('empleados.detalles')->with("empleado" ,$empleado);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
